// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by data-importerjs.js.
import { name as packageName } from "meteor/loco0321:data-importerjs";

// Write your tests here!
// Here is an example.
Tinytest.add('data-importerjs - example', function (test) {
  test.equal(packageName, "data-importerjs");
});
