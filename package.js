Package.describe({
    name: 'loco0321:data-importerjs',
    version: '0.0.3',
    // Brief, one-line summary of the package.
    summary: 'Allow to import xlsx files in collections',
    // URL to the Git repository containing the source code for this package.
    git: 'https://leon0321@bitbucket.org/leon0321/data-importerjs.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Npm.depends({
    'path': '0.13.7',
    'fs': '0.0.2',
    'validator': '5.5.0'
});

Package.onUse(function (api) {
    api.versionsFrom('1.4.0.1');
    api.use('ecmascript');
    api.use('netanelgilad:excel@0.2.6');
    api.mainModule('data-importerjs.js');

    api.export('BaseImporter');

});

Package.onTest(function (api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('loco0321:data-importerjs');
    api.mainModule('data-importerjs-tests.js');
});
