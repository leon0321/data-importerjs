/**
 * Created by leon on 9/08/16.
 */

//import fs from 'fs'
//import path from 'path'
// VALIDATORS
import isEmail  from 'validator/lib/isEmail';
import isDecimal  from 'validator/lib/isDecimal';
import isInt  from 'validator/lib/isInt';
import isNumeric  from 'validator/lib/isNumeric';
import isURL  from 'validator/lib/isURL';

//let basepath = path.resolve('.').split('.meteor')[0];

let TYPE = ['xlsx', 'xls'];

export class BaseImporter {
    constructor(filePath, type) {
        this.filePath = filePath;
        this.sheetIndex = 0;
        this.start_line = 1;
        this.errors = [];
        this.model = null;
        this.type = this.setType(type);
        this.validators = this.getValidators();
        this.fields = this.getFields();
        this.data = [];
    }

    getValidator(fieldName) {
        let validatorNameList = this.fields[fieldName]['validator'];
        let validatorList = [];
        for (let i in validatorNameList) {
            validatorList.push(this.validators[validatorNameList[i]]);
        }
        return validatorList;
    }

    getValidators() {
        return {'NUMERIC': isNumeric, 'DECIMAL': isDecimal, 'URL': isURL, 'INTEGER': isInt, 'EMAIL': isEmail};
    }

    getFields() {
        /**
         * Example:
         * let fields = {
         *   'name': {col: 0, validator: ['INTEGER']},
         *   'price': {col: 1, validator: ['DECIMAL']},
         *   'total': {col: 2, validator: ['DECIMAL']},
         *   'email': {col: 4, validator: ['EMAIL']},
         *   'description': {col: 4},
         * };
         */
        return {};
    }

    getHeader() {
        let header = [];
        for (let fieldName in  this.fields) {
            let col = this.fields[fieldName].col;
            header[col] = fieldName;
        }
        return header;
    }

    setType(type) {
        if (!type) {
            type = 'xlsx';
        }
        if (TYPE.indexOf(type) == -1) {
            throw new Error('type must be XLSX OR XLS');
        }
        return type;
    }

    readFile() {
        let excel = new Excel(this.type);
        let workbook = excel.readFile(this.filePath);
        let yourSheetsName = workbook.SheetNames;
        let sheet = workbook.Sheets[yourSheetsName[this.sheetIndex]];
        let options = {header: this.getHeader()};
        let data =  excel.utils.sheet_to_json(sheet, options);
        return data.slice(this.start_line)
    }

    clean() {
        let dataCleaned = [];
        for (let i = 0, row; row = this.data[i]; i++) {
            row = this.cleanRow(row, i);
            if (row) {
                dataCleaned.push(row);
            }
        }
        return dataCleaned
    }

    cleanRow(row, index) {
        let errors = [];
        for (let fieldName in this.fields) {
            let value = row[fieldName];
            if(isNaN(value)){
                value == ''
            }
            try {
                row[fieldName] = this.cleanField(fieldName, value, row);
            } catch (error) {
                errors.push({'error': `${error}`, 'field': fieldName, 'value': value});
            }
        }
        if (errors.length > 0) {
            // plus one to fix with excel's rows
            this.errors.push({'row': index + this.start_line + 1, 'errors': errors});
            return null
        }
        return row;
    }

    cleanField(fieldName, value, data_row) {
        let fieldNameCcase = fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
        let cleanMethod = this[`cleanField${fieldNameCcase}`];
        if (cleanMethod) {
            return this[`cleanField${fieldNameCcase}`](value, data_row);
        } else {
            let validators = this.getValidator(fieldName);
            for (let i in validators) {
                if (!validators[i](value)) {
                    let validatorName = this.fields[fieldName]['validator'][i];
                    throw `col: ${fieldName}; Error: ${validatorName}`;
                }
            }
            return value
        }
    }

    save(cleanedData) {
        if (!this.model) {
            throw 'this.model must be a Mongo schema';
        }
        for (let i = 0, data; data = cleanedData[i]; i++) {
            this.model.insert(data);
        }
    }

    start() {
        this.data = this.readFile();
        this.data = this.clean();
        this.save(this.data);
        return this.errors;
    }
}